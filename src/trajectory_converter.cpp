/* Copyright 2015 Sanjiban Choudhury
 * path_xyzv.h
 *
 *  Created on: Mar 7, 2015
 *      Author: Sanjiban Choudhury
 */
#include "ca_nav_msgs/trajectory_converter.h"
#include "Eigen/Core"

ca_nav_msgs::PathXYZVPsi ca_nav_msgs::ConvertViewPointPath2XYZVPsi(ca_nav_msgs::PathXYZVViewPoint &ip_path){
    ca_nav_msgs::PathXYZVPsi op_path;
    ca_nav_msgs::XYZVPsi op_wp;
    ca_nav_msgs::XYZVViewPoint ip_wp;

    double param_time_trajectory = 0.5;
    for(size_t i=0; i< ip_path.waypoints.size();i++){
        ip_wp = ip_path.waypoints[i];
        op_wp.position = ip_wp.position;
        op_wp.vel = ip_wp.vel;

        if(ip_wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_VIEWPOINT){
            double v1_x = ip_wp.viewpoint.x - ip_wp.position.x;
            double v1_y = ip_wp.viewpoint.y - ip_wp.position.y;
            op_wp.heading = std::atan2(v1_y,v1_x);
            op_path.waypoints.push_back(op_wp);
        }
        else if(ip_wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_FORWARD){
            if(i>0){
                //ca_nav_msgs::XYZVViewPoint& ip_wp_next = ip_path.waypoints[i+1];
                //double v1_x = -ip_wp.position.x + ip_wp_next.position.x;
                //double v1_y = -ip_wp.position.y + ip_wp_next.position.y;
                //op_wp.heading = std::atan2(v1_y,v1_x);

                Eigen::Vector3d prev_pos;
                prev_pos.x()= ip_path.waypoints[i-1].position.x;
                prev_pos.y()= ip_path.waypoints[i-1].position.y;
                prev_pos.z()= ip_path.waypoints[i-1].position.z;

                Eigen::Vector3d curr_pos;
                curr_pos.x()= ip_path.waypoints[i].position.x;
                curr_pos.y()= ip_path.waypoints[i].position.y;
                curr_pos.z()= ip_path.waypoints[i].position.z;

                Eigen::Vector3d vec = curr_pos - prev_pos;
                op_wp.heading = std::atan2(vec.y(),vec.x());
                double velocity = op_wp.vel;
                double tot_time = vec.norm()/velocity;
                Eigen::Vector3d new_pos = prev_pos + param_time_trajectory*vec/tot_time;
                op_wp.position.x = new_pos.x();
                op_wp.position.y = new_pos.y();
                op_wp.position.z = new_pos.z();
                op_path.waypoints.push_back(op_wp);
                op_wp.heading = std::atan2(vec.y(),vec.x());
                op_wp.position = ip_wp.position;
                op_path.waypoints.push_back(op_wp);


            }
            else{

            }
        }
        else if(ip_wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_FOV){
            if(i>0){
                ca_nav_msgs::XYZVViewPoint& ip_wp_prev = ip_path.waypoints[i-1];
                double vin_x = ip_wp.position.x - ip_wp_prev.position.x;
                double vin_y = ip_wp.position.y - ip_wp_prev.position.y;
                double vin_z = ip_wp.position.z - ip_wp_prev.position.z;
                Eigen::Vector3d pos(ip_wp_prev.position.x,ip_wp_prev.position.y,
                                    ip_wp_prev.position.z);
                Eigen::Vector3d vec(vin_x,vin_y,vin_z);
                double distance = vec.norm(); vec.normalize();
                double time_ahead = distance/ip_wp.vel;
                double heading_per_time = ip_wp.heading_rate*(ip_wp.max_heading - ip_wp.min_heading)/time_ahead;
                for(double j=param_time_trajectory;j<time_ahead; j=j+param_time_trajectory){
                    op_wp.position.x = pos.x() + j*ip_wp.vel*vec.x();
                    op_wp.position.y = pos.y() + j*ip_wp.vel*vec.y();
                    op_wp.position.z = pos.z() + j*ip_wp.vel*vec.z();
                    op_wp.heading = ip_wp.min_heading + j*heading_per_time;
                    op_path.waypoints.push_back(op_wp);
                }
            }
        }

    }

    for(size_t i=0; i< op_path.waypoints.size()-1;i++){
        geometry_msgs::Point p = op_path.waypoints[i].position;
        geometry_msgs::Point p_next = op_path.waypoints[i+1].position;
        Eigen::Vector3d pos((p.x - p_next.x), (p.y - p_next.y), (p.z - p_next.z));
        if(pos.norm()<0.05)
            op_path.waypoints.erase((op_path.waypoints.begin() + i));
    }

    op_path.header = ip_path.header;

    return op_path;
}
