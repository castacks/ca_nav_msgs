/* Copyright 2015 Sanjiban Choudhury
 * path_xyzv.h
 *
 *  Created on: Mar 7, 2015
 *      Author: Sanjiban Choudhury
 */
#include "ca_nav_msgs/trajectory_converter.h"
#include "ros/ros.h"

ros::Publisher pathXYZVPsi_pub;

void PathCallback(const ca_nav_msgs::PathXYZVViewPoint::ConstPtr& msg)
{
    ca_nav_msgs::PathXYZVViewPoint ip_path = *msg;
    ca_nav_msgs::PathXYZVPsi op_path;
    op_path = ca_nav_msgs::ConvertViewPointPath2XYZVPsi(ip_path);
    pathXYZVPsi_pub.publish(op_path);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "trajectory_converter");
  ros::NodeHandle n("~");

  std::string ip_topic;
  std::string op_topic;

  if(!n.getParam("ip_topic", ip_topic)){
        ROS_ERROR_STREAM("Trajectory Converter could not get parameter ip_topic");
        exit(-1);
  }

  if(!n.getParam("op_topic", op_topic)){
        ROS_ERROR_STREAM("Trajectory Converter could not get parameter op_topic");
        exit(-1);
  }

  pathXYZVPsi_pub = n.advertise<ca_nav_msgs::PathXYZVPsi>(op_topic, 1000);
  ros::Subscriber pathXYZVViewPoint_sub = n.subscribe(ip_topic, 1000, PathCallback);

  ros::spin();
  return 0;
}
