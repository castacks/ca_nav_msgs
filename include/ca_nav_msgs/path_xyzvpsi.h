/* Copyright 2015 Sanjiban Choudhury
 * path_xyzv.h
 *
 *  Created on: Mar 7, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef CA_NAV_MSGS_INCLUDE_CA_NAV_MSGS_PATH_XYZVPSI_H_
#define CA_NAV_MSGS_INCLUDE_CA_NAV_MSGS_PATH_XYZVPSI_H_

#include <Eigen/Dense>

#include "ca_nav_msgs/PathXYZVPsi.h"
#include "geom_cast/geom_cast.hpp"

namespace ca {

struct XYZVPsi {
  Eigen::Vector3d position;
  double vel;
  double heading;

  ca_nav_msgs::XYZVPsi GetMsg() {
    ca_nav_msgs::XYZVPsi msg;
    msg.position = ca::point_cast<geometry_msgs::Point>(position);
    msg.vel = vel;
    msg.heading = heading;
    return msg;
  }

  void SetMsg(const ca_nav_msgs::XYZVPsi &msg) {
    position = ca::point_cast<Eigen::Vector3d>(msg.position);
    vel = msg.vel;
    heading = msg.heading;
  }
};

struct PathXYZVPsi {
  std::vector<XYZVPsi> path;

  ca_nav_msgs::PathXYZVPsi GetMsg() {
    ca_nav_msgs::PathXYZVPsi msg;
    for (auto it : path)
      msg.waypoints.push_back(it.GetMsg());
    return msg;
  }

  void SetMsg(const ca_nav_msgs::PathXYZVPsi &msg) {
    path.clear();
    for (auto it : msg.waypoints) {
      XYZVPsi wp;
      wp.SetMsg(it);
      path.push_back(wp);
    }
  }
};

}



#endif /* CA_NAV_MSGS_INCLUDE_CA_NAV_MSGS_PATH_XYZVPSI_H_ */
