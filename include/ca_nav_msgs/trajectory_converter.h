/* Copyright 2015 Sanjiban Choudhury
 * path_xyzv.h
 *
 *  Created on: Mar 7, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef CA_NAV_MSGS_TRAJECTORY_CONVERTER_H
#define CA_NAV_MSGS_TRAJECTORY_CONVERTER_H
#include "ca_nav_msgs/PathXYZVViewPoint.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include <math.h>

namespace ca_nav_msgs{
    ca_nav_msgs::PathXYZVPsi ConvertViewPointPath2XYZVPsi(ca_nav_msgs::PathXYZVViewPoint &ip_path);
}
#endif
